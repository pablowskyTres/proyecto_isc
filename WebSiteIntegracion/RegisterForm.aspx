﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RegisterForm.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">Registro ISC</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" runat="server">
                        <fieldset>
                            <div class="form-group">
                                <label for="inputRut" class="col-lg-2 control-label">RUT</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputRut" runat="server" CssClass="form-control" TextMode="SingleLine" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputName" runat="server" CssClass="form-control" TextMode="SingleLine" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputLastName" class="col-lg-2 control-label">Apellido</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputLastName" runat="server" CssClass="form-control" TextMode="SingleLine" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputEmail" runat="server" CssClass="form-control" TextMode="SingleLine" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-lg-2 control-label">Contraseña</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputPassword" runat="server" CssClass="form-control" TextMode="Password" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword2" class="col-lg-2 control-label">Repita Contraseña</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputPassword2" runat="server" CssClass="form-control" TextMode="Password" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Ingresar" />
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="afterBody" Runat="Server">
</asp:Content>

