﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UploadService.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">Subir Servicio</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" runat="server">
                        <fieldset>
                            <div class="form-group">
                                <label for="txtTitulo" class="col-lg-2 control-label">Titulo</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="txtTitulo" runat="server" CssClass="form-control" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="select" class="col-lg-2 control-label">Categoría</label>
                                <div class="col-lg-10">
                                    <select class="form-control" id="selCategoria">
                                        <option>Seleccionar categoria</option>
                                        <option>Construccion</option>
                                        <option>Carpinteria</option>
                                        <option>Plomeria</option>
                                        <option>Mecanica Automotriz</option>
                                        <option>Finanzas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtDescripcion" class="col-lg-2 control-label">Descripción</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" rows="3" id="txtDescripcion" runat="server"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Subir Servicio" />
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="afterBody" Runat="Server">
</asp:Content>

