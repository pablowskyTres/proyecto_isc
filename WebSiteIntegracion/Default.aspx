﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            <h1>Error 404: Not Found</h1>
            <p>No se ha encontrado el recurso</p>
        </div>
        <div class="col-md-3"></div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="afterBody" Runat="Server">
</asp:Content>