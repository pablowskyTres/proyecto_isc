﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">

    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">Ingreso APP</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" runat="server">
                        <fieldset>
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputEmail" runat="server" CssClass="form-control" TextMode="SingleLine" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-lg-2 control-label">Contraseña</label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="inputPassword" runat="server" CssClass="form-control" TextMode="Password" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Ingresar" />
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="afterBody" Runat="Server">
</asp:Content>

